import psycopg2
import json
import asyncio
import conn_DB

from conn_DB import conn, cur



def excursion():
    
    
    async def insert_excursion():
        while True:
            name = input("Введите название экскурсии: ")    
            await conn_DB.insert_excursion(name)
            again = input("Добавить еще Экскурсию? ")
            if again in "yes" or again in 'да':
                continue
            else:
                break
    
    async def select_excursion():
        data_excursion = await conn_DB.select_excursion()
        for excursion in data_excursion:
            print(excursion)
            print("\n-----\n")
        
    

    choose = input("1-Добавить\n2-Посмотреть\n3-Удалить")
    
    if choose == "1":
        asyncio.run(insert_excursion())
    elif choose == "2":
        asyncio.run(select_excursion())
    elif choose == "3":
        asyncio.run(delete_excursion())
        
        
def question():
    
    
    
    async def select_questions():
        
        data_excursion = await conn_DB.select_excursion()
        for excursion in data_excursion:
            print(excursion)
            print("\n-----\n")
            
            
        number = input("Вопросы какой экскурсии(номер-id) вас интересуют: ")
        data_questions = await conn_DB.select_questions(number)
        for question in data_questions:
            print(question)
            print("\n-----\n")
    
    
    async def delete_question():
        q_id = input("Под каким номером удалить вопрос?")
        await conn_DB.delete_question()
            
            
            
        
     
    async def insert_question():
        while True:
            name = input("Введите вопрос: ")    
            i = 0
            question = input(f"Введите вариант ответа, если ответы коничлись отправь 'q':\n{i} - ")
            question_array = []
            while question != "q":
                question_array.append(question)
                i += 1
                question = input(f"Введите вариант ответа, если ответы коничлись отправь 'q': \n{i} - ")
            number_answer = int(input("Введите номер ответа: "))
            correct_answer = question_array[number_answer]
            excrusion_id = int(input("Введите номер экскурсии, к котрой принадлежит вопрос: "))
            print(f"{name} --- {question_array} | {correct_answer} - {excrusion_id}")
            print("все верно? ")
            ans = input()
            if ans in "yes":
                await conn_DB.insert_question(name, question_array,correct_answer, excrusion_id)
            again = input("Добавить еще вопрос? ")
            if again in "yes" or again in 'да':
                continue
            else:
                break
        
    choose = input("1-Добавить\n2-Посмотреть\n3-Удалить")
    if choose == "1":
        asyncio.run(insert_question())
    elif choose == "2":
        asyncio.run(select_questions())     
    elif choose == "3":
        asyncio.run(delete_question())


def admin():
    
    async def insert_admin():
        while True:
            user_id = input("Введите id пользователя: ")
            user_name = input("Введите username пользователя: ")
            await insert_to_admin(user_id, user_name)
            again = input("Добавить еще Админа? ")
            if again in "yes" or again in 'да':
                continue
            else:
                break
        
    
    choose = input("1-Добавить\n2-Посмотреть\n3-Удалить")
    if choose == "1":
        asyncio.run(insert_admin())
    elif choose == "2":
        asyncio.run(select_admin())     
    elif choose == "3":
        asyncio.run(delete_admin())



if __name__ == '__main__':
    choose = int(input("С чем будем работать?\n1 - Excursion\n2 - Question\n3 - Admin -->: "))
    try:
        if choose == 1:
            excursion()
        elif choose == 2:
            question()      
        elif choose == 3:
            admin()
    finally:
        cur.close()
        conn.close()
                