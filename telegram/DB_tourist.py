from psycopg2._json import Json
from DB import conn, cur
import json





async def select_user_id(user_id,excursion_key):
    cur.execute(
        f"SELECT id, name_user, points FROM Users_excursion WHERE excursion_id = {excursion_key}"
    )
    return cur.fetchall()

async def update_questions_users(users, question_id):
    cur.execute(
        "UPDATE Questions SET users_in_questions = '%s WHERE id = %s;",(json.dumps(users), question_id)
    )
    conn.commit()


async def update_users(user_id, excursion_key, points=0):
    cur.execute(f'UPDATE Users_excursion SET excursion_id = {excursion_key}, points = {points} WHERE id = {user_id};')
    conn.commit()


async def insert_user(user_id, user_name, excursion_key, points=0):
    cur.execute(
            'INSERT INTO Users_excursion (id, name_user, points, excursion_id) VALUES (%s, %s, %s, %s) ON CONFLICT (id) DO UPDATE SET name_user = %s, points = %s, excursion_id = %s',
            (user_id, user_name, 0, excursion_key, user_name, 0, excursion_key)
        )
    conn.commit()













# async def select_admins(user_id):
#     cur.execute(
#         'SELECT id FROM Admins WHERE id = %s',(user_id,)
#     )
#     return cur.fetchone()




# async def update_excursion_questions(questions_in_excursion, excursion_id):
#     cur.execute('UPDATE Excursion SET questions_in_excursion = %s WHERE id = %s', (json.dumps(questions_in_excursion), excursion_id))
#     conn.commit()    

# async def update_excursion_users(users_in_excursion, excursion_id):
#     cur.execute('UPDATE Excursion SET users_in_excursion = %s WHERE id = %s', (json.dumps(users_in_excursion), excursion_id))
#     conn.commit()    

# async def insert_excursion(excursion_name, users_in_excursion=Json([]), questions_in_excursion=Json([])):
#     cur.execute(
#             f'INSERT INTO excursion (excursion_name, users_in_excursion, questions_in_excursion) VALUES ({excursion_name}, {users_in_excursion}, {questions_in_excursion});'
#         )
#     conn.commit()





# async def select_excursion_name():
#     cur.execute(
#         f'SELECT id, excursion_name FROM Excursion'
#     )
#     return cur.fetchall()

# async def select_excursion_where_id(excrusion_id):
#     cur.execute(
#         f'SELECT excursion_name FROM Excursion WHERE id = {excrusion_id}'
#     )
#     return cur.fetchall()

# async def select_excursion(excursion_key):
#     cur.execute(
#         f'SELECT excursion_name, users_in_excursion, questions_in_excursion FROM Excursion WHERE id = {excursion_key}',
#     )
#     return cur.fetchall()   


# async def select_questions_question_id( question_id):
#     cur.execute(
#         f'SELECT questions_name, users_in_questions, answers_in_questions, correct_answer FROM questions WHERE question_id = {question_id}'
#     )
#     return cur.fetchall()



# async def select_questions_id(excursion_id, question_id):
#     cur.execute(
#         f'SELECT questions_name, users_in_questions, answers_in_questions, correct_answer FROM questions WHERE excursion_id = {excursion_id} AND id = {question_id}'
#     )
#     return cur.fetchall()


# async def update_questions(id, question_id):
#     cur.execute(
#         f'UPDATE Questions SET question_id = {question_id} WHERE id = {id}'
#     )
#     conn.commit()

# async def update_questions_users(users, question_id):
#     cur.execute(
#         f'UPDATE Questions SET users_in_questions = {json.dumps(users)} WHERE id = {question_id}'
#     )
#     conn.commit()


# async def select_questions(excursion_id):
#     cur.execute(
#         f'SELECT id, questions_name, correct_answer, question_id FROM questions WHERE excursion_id = {excursion_id} '
#     )
#     return cur.fetchall()


# async def select_excrusion_questions(excursion_id):
#     cur.execute(
#     f'SELECT questions_in_excursion FROM Excursion WHERE id = {excursion_id} '
#     )
#     return cur.fetchall()






# async def select_users(excursion_key):
#     cur.execute(
#         f"SELECT id, name_user, points FROM Users_excursion WHERE excursion_id = {excursion_key}"
#     )
#     return cur.fetchall()

# async def select_user_id(id_user, excursion_key):
#     cur.execute(
#         f"SELECT points FROM Users_excursion WHERE excursion_id = {excursion_key} AND id = {id_user}"
#     )
#     return cur.fetchall()





# async def delete_user(id):
#     cur.execute(f"DELETE FROM Users_excursion WHERE id = {id};")
#     conn.commit()

# async def delete_user_excursion(excursion_id):
#     cur.execute(f"DELETE FROM Users_excursion WHERE excursion_id = {excursion_id};")
#     conn.commit()
