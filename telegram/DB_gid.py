
from DB import conn, cur
import json

async def update_admins(excursion_key, user_id, user_name):
    cur.execute('UPDATE Admins SET excursion_id = %s WHERE id = %s AND name_user = %s;',(excursion_key, user_id, user_name))
    conn.commit()


async def update_questions_in_excursion(questions_in_excursion, excursion_id):
    cur.execute("UPDATE Excursion SET questions_in_excursion = %s WHERE id = %s", (json.dumps(questions_in_excursion), excursion_id))
    conn.commit()   


async def update_questions(id, question_id):
    cur.execute(
        f'UPDATE Questions SET question_id = {question_id} WHERE id = {id}'
    )
    conn.commit()

async def delete_excursion(id):
    cur.execute(f"DELETE FROM Excursion WHERE id = {id};")
    conn.commit()