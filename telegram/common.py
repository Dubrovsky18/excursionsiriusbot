
from aiogram import Router 
from aiogram.filters import Command, Text
from aiogram.fsm.context import FSMContext
from aiogram.types import Message, ReplyKeyboardRemove

import difflib
import re

from DB import select_admins, select_excursion, select_users, select_questions, select_excursion_key
from DB import select_questions_id



router = Router()


common_button = ['/list', '/statistic', '/help']



def check_answer(answer, correct_answer):
    # Нормализуем строки и убираем все не буквенно-цифровые символы
    answer = re.sub(r'\W+', '', answer.lower())
    correct_answer = re.sub(r'\W+', '', correct_answer.lower())
    # Используем модуль difflib для проверки схожести строк
    similarity_ratio = difflib.SequenceMatcher(None, answer, correct_answer).ratio()
    return similarity_ratio >= 0.8  



# help command
@router.message(Command(commands=["help"]))
async def help_message(message: Message):
    await message.answer('''Привет, я бот-экскурсовод:
    \n\nВы можете выбрать вопрос, на который туристы ддолжны будут отвечать.
    \n Что узнать список доступных вопросов, нажмите /list''')


@router.message(Command(commands=["start"]))
async def start_command(message: Message, state: FSMContext):
    await state.clear()
    # Получаем id пользователя и проверяем, есть ли он в базе данных
    user_id, user_name = message.from_user.id, message.from_user.username
    user_db = await select_admins(user_id=user_id)
    result = await show_excursions()
    if user_db:
        from gid import Gid
        # Если пользователь найден в базе данных, он является экскурсоводом
        await message.answer(text=f"Я бот-экскурсовод и вы экскурсовод. \n Узнай обо мне - /help \n Выберите эскурсию\n\n{result}")
        await state.set_state(Gid.waiting_for_excursion)
    else:
        # Если пользователь не найден в базе данных, он является туристом
        from tourist import Tourist
        await message.answer(text=f"Я бот-экскурсовод и вы турист. Введите номер экскурсии")
        await state.set_state(Tourist.waiting_for_excursion)



@router.message(Command(commands=["statistic"]))
async def button_statistic(message: Message, state: FSMContext):
    data = await state.get_data()
    excursion_key = data.get("excursion_key")
    if excursion_key != None:
        excursion = await select_excursion()
        if excursion is not None and len(excursion) != 0:
            excursion_name = excursion[0][1]
            mes = (f"Эскурсия {excursion_name} - {excursion_key}\n"
            f"User Id \t User Name \t Points")
            users = await select_users(excursion_key)
            if users is not None:
                for user in users:
                    user_id = user[0]
                    user_name = user[1]
                    user_points = user[2]
                    mes += f"\n{user_id} \t {user_name} \t {user_points}"
                await message.answer(text=mes)
            else:
                await message.answer(text="Пользователи не найдены ")
        else:
            await message.answer(text="Выберите экскурсию")
    else:
        await message.answer(text="Экскурсия не найдена")



async def show_excursions():
    excursions = await select_excursion()
    if excursions is not None and len(excursions) != 0:
        result = "Доступные экскурсии"
        for excursion in excursions:
            id = str(excursion[0])
            item = excursion[1]
            result += f"\n/{id} - {item}"
        return result
    else:
        return "! Доступных экскурсий нет"


async def show_questions(excursion_key):
    questions = await select_questions(excursion_key)
    if questions is not None and len(questions) != 0:
        result = "Доступные вопросы"
        for question in questions:
            id = question[0]
            item = question[1]
            result += f"\n/{id} - {item}"
        return result
    else:
        return "! Неверый номер вопроса"
    
async def show_questions_id(excursion_key):
    questions = await select_excursion_key(excursion_key=excursion_key)
    if questions is not None and len(questions) != 0:
        if len(questions[0][2]) != 0:
            result = "Доступные вопросы"
            for question in questions[0][2]:
                result += f"\n/{question}"
            return result
        else:
            return "! Вопросов нет"
    else:
        return "! Экскурсии такой нет"

async def question(excursion_key, question_id):
    question = await select_questions_id(excursion_key,question_id)
    if question is not None and len(question) != 0:
        question_name = question[0][0]
        answers = question[0][2]
        result = f"Вопрос\n{question_name}\n\nВарианты ответа"
        for i in range(len(answers)):
            item = answers[i]
            result += f"\n/{i} - {item}"
        return result
    else:
        return "! Неверый номер вопроса."
