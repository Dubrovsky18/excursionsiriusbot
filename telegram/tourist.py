from aiogram import Router
from aiogram.types import Message
from aiogram.filters import Command
from aiogram.fsm.state import State, StatesGroup
from aiogram.fsm.context import FSMContext


import logging
from DB_tourist import select_user_id, update_users, update_questions_users, insert_user
from DB import select_excursion_key, select_questions_id, update_excursion_users
from common import show_questions_id, check_answer



class Tourist(StatesGroup):
    waiting_for_excursion = State()
    waiting_for_tourist_answer = State()
    waiting_for_tourist = State()


router = Router()
logger = logging.getLogger(__name__)


@router.message(Command(commands=["list"]), Tourist.waiting_for_tourist)
async def button_list(message: Message, state : FSMContext):
    data = await state.get_data()
    excursion_key = data.get("excursion_key")
    if excursion_key != None:
        result = await show_questions_id(excursion_key=excursion_key)
        await message.answer(text=result)
        await state.set_state(Tourist.waiting_for_tourist)
    else:
        await message.answer(text="Введите номер экскурсии")
        await state.set_state(Tourist.waiting_for_excursion)


@router.message(Tourist.waiting_for_excursion)
async def choose_excursion(message: Message, state: FSMContext):
    excursion_key = message.text
    user_id, user_name = message.from_user.id, message.from_user.username
    try:
        excursion_key = int(excursion_key)
    except Exception:
        await message.answer("Неверный номер экскурсии")
        await state.set_state(Tourist.waiting_for_excursion)
    else:
        excursion_data = await select_excursion_key(excursion_key)
        if excursion_data is not None and len(excursion_data) != 0:
            await state.set_state(Tourist.waiting_for_tourist)
            await insert_user(user_id, user_name, excursion_key)
            await message.reply(f"Ты присоединился к экскурсии {excursion_data[0][1]}\n"
                                "Чтобы увидеть доступные вопроса нажми:\n/list")
            await state.update_data(excursion_key=excursion_key)
            users_in_excursion = excursion_data[0][2]
            if user_id not in users_in_excursion:
                users_in_excursion.append(user_id)
            await update_excursion_users(users_in_excursion, excursion_key)
            await update_users(user_id=user_id,excursion_key=excursion_key)
            await state.set_state(Tourist.waiting_for_tourist)
        else:
            await message.reply("Такой экскурсии нет\n"
                                "Попробуй еще раз")
            await state.set_state(Tourist.waiting_for_excursion)
    
    
    
@router.message(Tourist.waiting_for_tourist)
async def choose_question(message: Message, state: FSMContext):
    question_id = message.text
    user_id = message.from_user.id
    data = await state.get_data()
    excursion_key = data.get("excursion_key")
    if question_id[0] != "/":
        await message.reply("Нажмите на ссылку с номером")
        await state.set_state(Tourist.waiting_for_tourist)
    else:
        question_id = question_id[1:]
        questions_data= await select_questions_id(excursion_key=excursion_key, question_id=question_id)
        if questions_data is not None:
            print(questions_data[0][4])
            if user_id in questions_data[0][4]:
                await message.answer("Вы уже отвечали на этот вопрос")
                await state.set_state(Tourist.waiting_for_tourist)
            else:
                await state.update_data(question_key=question_id)
                questions_name = questions_data[0][1]
                answers = questions_data[0][2]
                result = "Варианты ответов:"
                for i in range(len(answers)):
                        result += f"\n/{i} - {answers[i]}"
                await message.answer(f'''Вопрос:\n
                {questions_name}\n
                {result}
                ''')
                await state.set_state(Tourist.waiting_for_tourist_answer)
        else:
            await state.set_state(Tourist.waiting_for_tourist)
            await message.answer(text="Такого ключа вопроса еще нет")


@router.message(Tourist.waiting_for_tourist_answer)
async def choose_answer(message: Message, state: FSMContext):
    user_name, user_id = message.from_user.username, message.from_user.id
    data = await state.get_data()
    excursion_key = data.get("excursion_key")
    question_key = data.get("question_key")

    answer = message.text
    if answer[0] != "/":
        await message.reply("Нажмите на ссылку с номером")
        await state.set_state(Tourist.waiting_for_tourist_answer)
    else:
        answer = int(answer[1:])

        question_data = await select_questions_id(excursion_key=excursion_key, question_id=question_key)

        if answer > len(question_data[0][3]):
            await message.reply("Выходи за рамки вариантов")
            await state.set_data(Tourist.waiting_for_tourist_answer)
        else:
            correct_answer = question_data[0][3]
            answer = question_data[0][2][int(answer)]

            user_data = await select_user_id(user_id, excursion_key)
            points = user_data[0][2]

            if check_answer(answer=answer, correct_answer=correct_answer):
                points +=1

            question_data = await select_questions_id(excursion_key=excursion_key, question_id=question_key)
            if question_data is not None and len(question_data) != 0:
                users_in_questions = question_data[0][4]
                if user_id not in users_in_questions:
                    users_in_questions.append(user_id)
                await update_questions_users(users=users_in_questions, question_id=question_key)
                question_data = await select_questions_id(excursion_key=excursion_key, question_id=question_key)
                print(question_data[0][4])
                await update_users(user_id=user_id,  excursion_key=excursion_key, points=points)
                await message.reply("Ваш ответ принят!")
                await state.set_state(Tourist.waiting_for_tourist)
            else:
                await message.answer("Произошла ошибка. Попробуй позже")
                await state.set_state(Tourist.waiting_for_tourist)

@router.message(Command(commands=["end"]), Tourist.waiting_for_tourist)
async def end_tour(message: Message, state: FSMContext):
    user_id = message.from_user.id
    await message.answer("Вы покинули экскурсию.")
    await state.finish()
