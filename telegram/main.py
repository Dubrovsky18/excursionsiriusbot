from aiogram import Bot, Dispatcher
from aiogram.fsm.storage.memory import MemoryStorage
import asyncio


from DB import create_DB, cur, conn
import common
import logging
import os



# bot = Bot(token="6149236773:AAESVw8YQ0D55qAU23Fltmmn8L-Lxh5FIYo")
bot = Bot(os.environ['BOT_TOKEN'])


async def main():

    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s - %(levelname)s - %(name)s - %(message)s",
    )


    import tourist, gid
    dp = Dispatcher(storage=MemoryStorage())
    dp.include_router(common.router)
    dp.include_router(tourist.router)
    dp.include_router(gid.router)



    await dp.start_polling(bot, allowed_updates=dp.resolve_used_update_types())

if __name__ == '__main__':
    try:
        create_DB()
        asyncio.run(main())
    finally:
        cur.close()
        conn.close()