from aiogram import Router
from aiogram.types import Message
from aiogram.filters import Command
from aiogram.fsm.state import State, StatesGroup
from aiogram.fsm.context import FSMContext

import logging
import random

from simple_row import make_row_keyboard
from main import bot
from common import show_excursions, show_questions
from common import common_button
from DB import select_excursion, select_questions, select_questions_key, select_exit_questions
from DB_gid import update_admins, update_questions_in_excursion, update_questions


class Gid(StatesGroup):
    waiting_for_excursion = State()
    waiting_for_gid = State()
    waiting_for_show_excrsion = State()


router = Router()
logger = logging.getLogger(__name__)



@router.message(Command(commands=["list"]), Gid.waiting_for_gid)
async def button_list(message: Message, state: FSMContext):
    data = await state.get_data()
    excursion_key = data.get("excursion_key")
    if excursion_key != None:
        result = await show_questions(excursion_key=excursion_key)
        await message.answer(text=result)
        await state.set_state(Gid.waiting_for_gid)
    else:
        result = await show_excursions()
        await message.answer(text=result)
        await state.set_state(Gid.waiting_for_excursion)


@router.message(Gid.waiting_for_excursion)
async def choose_excursion_name(message: Message, state: FSMContext):
    user_name, user_id = message.from_user.username, message.from_user.id
    excursion_key = message.text
    if excursion_key[0] != "/":
        await message.reply("Нажмите на ссылку с номером")
        result_excursion = await show_excursions()
        if result_excursion[0] != "!":
            await message.answer(text=result_excursion)
            await state.set_state(Gid.waiting_for_excursion)
    else:
        try:
            excursion_key = int(excursion_key[1:])
        except Exception:
            logger.exception(f"Ошибка от {user_name} - {user_id}")
            await message.answer(text="Произошла ошибка при выполнении команды.\n"
                                 "Попробуй еще раз")
            await state.set_state(Gid.waiting_for_excursion)
        else:
            await state.update_data(excursion_key=excursion_key)
            await update_admins(excursion_key=excursion_key, user_id=user_id, user_name=user_name)
            await message.answer(text="Экскурсия началась")
        finally:
            await state.set_state(Gid.waiting_for_gid)


@router.message(Command(commands=['end']),Gid.waiting_for_gid)
async def button_end(message: Message, state: FSMContext):
    user_id, user_name = message.from_user.id, message.from_user.username
    data = await state.get_data()
    excursion_key = data.get("excursion_key")

    users_in_excursion_json = await select_excursion()

    for user in users_in_excursion_json[2]:
        await bot.send_message(chat_id=user, text="Эскурсия завершена\n"
                               "нажмите /end ")
    await message.answer("Вы завершили эскурсию")
    await update_admins(excursion_key=excursion_key,user_id=user_id, user_name=user_name)
    await state.finish()

