import json
import psycopg2
import os

conn = psycopg2.connect(
        host=os.environ['POSTGRES_HOST'],
        port=os.environ['POSTGRES_PORT'],
        database=os.environ['POSTGRES_DB'],
        user=os.environ['POSTGRES_USER'],
        password=os.environ['POSTGRES_PASSWORD']
    )

# conn = psycopg2.connect(
#         host="localhost",
#         port="3333",
#         database="postgres",
#         user="postgres",
#         password="123"
#     )

cur = conn.cursor()

def create_DB():
    cur.execute('''

    CREATE TABLE IF NOT EXISTS Excursion (
        id bigint generated always as identity primary key,
        excursion_name text,
        users_in_excursion jsonb,
        questions_in_excursion jsonb
    );

    drop table if exists Users_excursion cascade;
    CREATE TABLE Users_excursion (
        id bigint PRIMARY KEY,
        name_user text,
        points int,
        excursion_id bigint REFERENCES Excursion(id)
    );

    CREATE TABLE IF NOT EXISTS Questions (
        id bigint  generated always as identity primary key,
        name_question text,
        answers_in_questions jsonb,
        correct_answer text,
        excursion_id bigint REFERENCES Excursion(id),
        question_id bigint,
        users_in_questions jsonb
    );

    CREATE TABLE IF NOT EXISTS Admins (
        id bigint PRIMARY KEY,
        name_user text,
        excursion_id bigint REFERENCES Excursion(id)
    );

    INSERT INTO Admins (id, name_user, excursion_id) VALUES (%s, %s, %s)
    ON CONFLICT (id) DO NOTHING;
        ''', (426068591, "Dubrovsky_Igor", None))

    conn.commit()



async def select_admins(user_id):
    cur.execute(
        'SELECT id FROM Admins WHERE id = %s',(user_id,)
    )
    return cur.fetchone()

async def select_excursion():
    cur.execute(
        f'SELECT id, excursion_name, users_in_excursion, questions_in_excursion FROM Excursion')
    return cur.fetchall()



async def select_excursion_key(excursion_key):
    cur.execute(
        f'SELECT excursion_name, users_in_excursion, questions_in_excursion FROM Excursion WHERE id = {excursion_key}')
    return cur.fetchall()



async def select_users(excursion_key):
    cur.execute(
        f"SELECT id, name_user, points FROM Users_excursion WHERE excursion_id = {excursion_key}"
    )
    return cur.fetchall()

async def select_questions(excursion_key):
    cur.execute(
        f'SELECT id, name_question, answers_in_questions, correct_answer, question_id, users_in_questions FROM questions WHERE excursion_id = {excursion_key} '
    )
    return cur.fetchall()

async def select_questions_key(excursion_key, question_key):
    cur.execute(
        f'SELECT id, name_question, answers_in_questions, correct_answer, question_id, users_in_questions FROM questions WHERE id = {question_key} and excursion_id = {excursion_key} '
    )
    return cur.fetchall()

async def select_questions_id(excursion_key, question_id):
    cur.execute(
        f'SELECT id, name_question, answers_in_questions, correct_answer, users_in_questions FROM Questions WHERE question_id = {question_id} and excursion_id = {excursion_key}'
    )
    return cur.fetchall()

async def select_exit_questions(excursion_key):
    cur.execute(
        f'''SELECT id, name_question, question_id
FROM Questions 
WHERE question_id != 0 AND excursion_id = {excursion_key};'''
    )
    return cur.fetchall()


async def update_excursion_users(users_in_excursion, excursion_key):
    cur.execute('UPDATE Excursion SET users_in_excursion = %s WHERE id = %s', (json.dumps(users_in_excursion), excursion_key))
    conn.commit()    
