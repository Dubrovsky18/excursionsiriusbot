from aiogram import Router
from aiogram.types import Message
from aiogram.filters import Command
from aiogram.fsm.state import State, StatesGroup
from aiogram.fsm.context import FSMContext
from aiogram.types import (CallbackQuery, InlineKeyboardButton,
                           InlineKeyboardMarkup, Message)


from aiogram.filters import state



import logging
from DB_tourist import select_user_id, update_users, update_questions_users, insert_user
from DB import select_excursion_key, select_questions_id, update_excursion_users, select_questions
from common import show_questions_id, check_answer



class Tourist(StatesGroup):
    waiting_for_excursion = State()
    waiting_for_tourist_answer = State()
    waiting_for_tourist = State()
    waiting_for_name = State()
    data_questions = []
    number = 0



router = Router()
logger = logging.getLogger(__name__)


@router.message(Tourist.waiting_for_name)
async def tourist_name(message: Message, state: FSMContext):
    user_name = message.text
    if user_name == "/end":
        await message.answer("Goodbay неизвестный\n\nЗахочешь поиграть нажми /start")
        await state.finish()
    await state.update_data(user_name=user_name)
    user_hello = user_name[:-3]
    await message.answer(text=f"Привет {user_hello}\n\nВВеди номер экскурсии, который тебе сообщили")
    await state.set_state(Tourist.waiting_for_excursion)
    
    
@router.message(Tourist.waiting_for_excursion)
async def choose_excursion(message: Message, state: FSMContext):
    excursion_key = message.text
    if excursion_key == "/end":
        await message.answer("Goodbay \n\nЗахочешь поиграть нажми /start")
        await state.finish()
    user_id = message.from_user.id
    data = await state.get_data()
    user_name = data.get("user_name")
    try:
        excursion_key = int(excursion_key)
    except Exception:
        await message.answer("Неверный номер экскурсии")
        await state.set_state(Tourist.waiting_for_excursion)
    else:
        excursion_data = await select_excursion_key(excursion_key)
        if excursion_data is not None and len(excursion_data) != 0:
            await insert_user(user_id, user_name, excursion_key)
            await message.reply(f"Ты присоединился к экскурсии \n'{excursion_data[0][0]}'")
            await state.update_data(excursion_key=excursion_key)
            Tourist.data_questions = await select_questions(excursion_key)
            users_in_excursion = excursion_data[0][2]
            if user_id not in users_in_excursion:
                users_in_excursion.append(user_id)
            await update_excursion_users(users_in_excursion, excursion_key)
            await update_users(user_id=user_id,excursion_key=excursion_key)
            mes = await show_question(excursion_key, Tourist.number, Tourist.data_questions)
            if mes == -1:
                await message.answer("Вопросы кончились \n узнай /statistic")
            else: 
                keyboard: list[list[InlineKeyboardButton]] = [
                    [InlineKeyboardButton(text='0', callback_data='/answer_0')],
                    [InlineKeyboardButton(text='1', callback_data='/answer_1')],
                    [InlineKeyboardButton(text='2', callback_data='/answer_2')]
                ]
                markup: InlineKeyboardMarkup = InlineKeyboardMarkup(inline_keyboard=keyboard)
                # Редактируем сообщение и клавиатуру
                await message.answer(text=mes,reply_markup=markup)
            await state.set_state(Tourist.waiting_for_tourist)
        else:
            
            await message.reply("Такой экскурсии нет\n"
                                "Попробуй еще раз")
            await state.set_state(Tourist.waiting_for_excursion)
            
            
async def show_question(excursion_key, number, questions):
    if number >= len(questions):
        return -1
    else:
        questions_name = questions[number][1]
        answers = questions[number][2]
        result = "Варианты ответов:"
        for i in range(len(answers)):
            result += f"\n{i} - {answers[i]}"
            
        return (f"Вопрос:\n{questions_name}\n{result}")

@router.callback_query(lambda c: c.data.startswith('/answer_'))
async def choose_answer_callback(callback_query: CallbackQuery, state: FSMContext):
    callback_data = callback_query.data
    user_id = callback_query.from_user.id
    # Извлечение номера ответа из callback_data
    try:
        answer = int(callback_data.split('_')[1])
    except Exception:
        await callback_query.answer("Неверный номер экскурсии")
        await state.set_state(Tourist.waiting_for_tourist_answer)

    data = await state.get_data()
    excursion_key = data.get("excursion_key")
    number = Tourist.number

    if answer >= len(Tourist.data_questions[number][2]):
        await callback_query.answer("Выход за рамки вариантов")
        await state.set_state(Tourist.waiting_for_tourist_answer)

    correct_answer = Tourist.data_questions[number][3]
    answer = Tourist.data_questions[number][2][answer]

    user_data = await select_user_id(user_id, excursion_key)
    points = user_data[0][2]

    if check_answer(answer=answer, correct_answer=correct_answer):
        points += 1

    users_in_questions = Tourist.data_questions[number][5]
    if user_id not in users_in_questions:
        users_in_questions.append(user_id)

    await update_questions_users(users=users_in_questions, question_id=Tourist.data_questions[number][0])
    await update_users(user_id=user_id,  excursion_key=excursion_key, points=points)
    number += 1
    Tourist.number = number
    await state.update_data(number=number)

    mes = await show_question(excursion_key, Tourist.number, Tourist.data_questions)
    if mes == -1:
        await callback_query.message.answer("Вопросы кончились \n узнай /statistic")
    else:
        # Создаем клавиатуру с кнопками для вариантов ответов
        options = Tourist.data_questions[number][2]
        keyboard = [[InlineKeyboardButton(text=str(i), callback_data=f'/answer_{i}')] for i in range(len(options))]
        markup: InlineKeyboardMarkup = InlineKeyboardMarkup(inline_keyboard=keyboard)
        # Редактируем сообщение и клавиатуру
        await callback_query.message.edit_text(text=mes,reply_markup=markup)

    await state.set_state(Tourist.waiting_for_tourist)
