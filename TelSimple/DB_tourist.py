from psycopg2._json import Json
from DB import conn, cur
import json





async def select_user_id(user_id,excursion_key):
    cur.execute(
        f"SELECT id, name_user, points FROM Users_excursion WHERE excursion_id = {excursion_key}"
    )
    return cur.fetchall()

async def update_questions_users(users, question_id):
    cur.execute(
        "UPDATE Questions SET users_in_questions = %s WHERE id = %s;",(json.dumps(users), question_id)
    )
    conn.commit()


async def update_users(user_id, excursion_key, points=0):
    cur.execute(f'UPDATE Users_excursion SET excursion_id = {excursion_key}, points = {points} WHERE id = {user_id};')
    conn.commit()


async def insert_user(user_id, user_name, excursion_key, points=0):
    cur.execute(
            'INSERT INTO Users_excursion (id, name_user, points, excursion_id) VALUES (%s, %s, %s, %s) ON CONFLICT (id) DO UPDATE SET name_user = %s, points = %s, excursion_id = %s',
            (user_id, user_name, 0, excursion_key, user_name, 0, excursion_key)
        )
    conn.commit()





