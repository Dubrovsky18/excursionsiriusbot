
import psycopg2
import json
import asyncio




conn = psycopg2.connect(
        host="localhost",
        port="3333",
        database="postgres",
        user="postgres",
        password="123"
    )

cur = conn.cursor()


async def insert_question(name_question, answers_in_questions, correct_answer, excrusion_id, question_id=0, users_in_questions=[]):
    cur.execute(f"INSERT INTO Questions (name_question, answers_in_questions,correct_answer,excursion_id,question_id,users_in_questions)"
                f" Values('{name_question}','{json.dumps(answers_in_questions)}', '{correct_answer}', {excrusion_id}, {question_id}, '{json.dumps(users_in_questions)}');")
    conn.commit()

async def insert_excursion(excursion_name,users_in_excursion=[],questions_in_excursion=[]):
    cur.execute(f"INSERT INTO Excursion (excursion_name,users_in_excursion,questions_in_excursion)"
                f" Values('{excursion_name}','{json.dumps(users_in_excursion)}', '{questions_in_excursion}');")
    conn.commit()
    
    
async def insert_admin(user_id, user_name, excursion_id=None):
    cur.execute(
    '''
    INSERT INTO Admins (id, name_user, excursion_id) VALUES (%s, %s, %s)
    ON CONFLICT (id) DO NOTHING;
        ''', (user_id, user_name, excursion_id))
    conn.commit()
    
    
async def select_excursion():
    cur.execute(
        f'SELECT id, excursion_name, users_in_excursion, questions_in_excursion FROM Excursion')
    return cur.fetchall()

async def select_questions(excursion_key):
    cur.execute(
        f'SELECT id, name_question, answers_in_questions, correct_answer, question_id, users_in_questions FROM questions WHERE excursion_id = {excursion_key} '
    )
    return cur.fetchall()

async def delete_question(question_id):
    cur.execute(
        f"DELETE FROM Questions WHERE id = {question_id};"
    )
    conn.commit()